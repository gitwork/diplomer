Структура репозитория

app - классы и функции
backend - реализация backend с помощью функций из app
cli - реализация cli с помощью функций из app
frontend - реализация frontend
test - тесты:

app - тесты для app
backend - тесты для backend
cli - тесты для cli




Установка pre-commit hook
Файл pre-commit необходимо скопировать в .git/hooks:

pip install black flake8 pylint
cp pre-commit .git/hooks/
chmod a+x .git/hooks/pre-commit