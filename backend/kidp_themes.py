"""
Module, thath contains methods to analyze, export, import and insert
issues into a temlate.
    As CLI:
        python3 -m kidp_themes.py [options]
        Options:
            -a, --action - str, from select: [validate, export, import, insert]
            -u, --url - str, url path. Example: http://gitlab.com/diplomer.git
            -t, --token - str, contains access-token
            -f, --file - str, file for import option with themes.W
"""
import os
import sys
import click

# pylint: disable=locally-disabled, import-error
from parse_themes import parse_issue


@click.option(
    "-a",
    "--action",
    prompt="Action for program",
    type=click.Choice(["validate", "export", "import", "insert"]),
    show_choices=True,
    required=True,
    help="Action for program. [validate, export, import, insert]",
)
@click.option(
    "-u",
    "--url",
    prompt="Url to project",
    type=str,
    required=True,
    help="Url to project. Example: http://gitlab.com/diplomer.git",
)
@click.option(
    "-t",
    "--token",
    prompt="Token for Gitlab api",
    type=str,
    required=True,
    help="Access token from site \
        https://gitlab.com/-/profile/personal_access_tokens",
)
@click.option(
    "-f",
    "--file",
    prompt="File with themes",
    type=str,
    default=None,
    help="File with themes for importing to project like issue",
)
def call_function(action: str, url: str, token: str, file: str):
    """Function to parse CLI args and calls a function

    Args:
        action (str): call a function
        url (str): url to project
        token (str): access token
        file (str): import file
    """
    actions = {"validate": parse_issue}

    if action == "import":
        if not os.path.isfile(file):
            sys.exit(-1)

    command = actions.get(action)
    command(url, token, file)


main = click.command()(call_function)

if __name__ == "__main__":
    main()
