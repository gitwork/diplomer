"""Module to get issues
"""
import re
from typing import List
from gitlab import Gitlab
from gitlab.v4.objects import Project


def parse_url(link: str) -> List[str]:
    """Funtiont to parse url to project

    Args:
        link (str): projet's url

    Returns:
        _type_: _description_
    """
    link = re.sub(r"/$", "", link)
    info = link.split("/")

    return info


def process_group(link: str, access_token: str) -> Project:
    """Funtion to get Project by link

    Args:
        link (str): link to project
        access_token (str): access token to get project

    Returns:
        Project: project with issues
    """
    url = parse_url(link)
    gitlink = "/".join(url[:3])
    user, projectname = url[3:]
    git = Gitlab(url=gitlink, private_token=access_token, ssl_verify=False)
    git.auth()

    project = git.projects.get(f"{user}/{projectname}")
    return project
