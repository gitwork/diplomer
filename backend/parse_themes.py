"""Module to parse issues
"""

# pylint: disable=locally-disabled, unused-argument, import-error

from dataclasses import dataclass
from enum import Enum
from re import sub, search
from typing import List, Dict
from gitlab.v4.objects import Issue

from process_group import process_group


class Labels(str, Enum):
    """Class for labels for issues"""

    DIPL = "диплом"
    KURS = "курсовая"
    UNKNOWN = "неизвестно"


class Warnings(str, Enum):
    """Class for warining to json"""

    MISSING = "Missing part: "
    TITLE = "Unknown title: "
    UNKNOWN = "Unknown description: "


@dataclass
class Title:
    """Dataclass for title parsing"""

    tag: str
    title: str
    warning: str


class IssueParser:
    """Class for parsing issues"""

    sep = "---"
    title_sep = "_"
    theme_sep = "__"
    title_start = "**"

    author = r"\*\*Руководитель.*\*\*"
    theme = r"\*\*Тема.*\*\*"
    target = r"\*\*Цель.*\*\*"
    quests = r"\*\*.*[зЗ]адачи.*\*\*"
    recomendation = r"\*\*Рекомендации.*\*\*"
    annotation = r"\*\*Аннотация.*\*\*"

    @staticmethod
    def parse_meta(issue: Issue) -> Dict:
        """Method to get meta info (title, url, labels) from issue

        Args:
            issue (Issue): issue with theme

        Returns:
            Dict: JSON-like with info from title
        """
        tag = IssueParser.__get_type(issue)
        url = issue.web_url
        labels = issue.labels

        info = issue.title.split(IssueParser.theme_sep)
        actors = info[0].split(IssueParser.title_sep) if len(info) > 1 else ""
        theme = sub("_", " ", info[1]) if len(info) > 1 else ""
        extra = info[2] if len(info) > 2 else ""

        author = actors[0] if len(actors) > 0 else ""
        student = actors[1] if len(actors) > 1 else ""

        return {
            "url": url,
            "tag": tag,
            "labels": labels,
            "title": {
                "author": author,
                "student": student,
                "theme": theme,
                "extra": extra,
            },
        }

    @staticmethod
    def parse_info(issue: Issue) -> Dict:
        """Method to get info from issue

        Args:
            issue (Issue): issue with theme
        Returns:
            Dict: info from issue
        """
        result = {}
        tag = IssueParser.__get_type(issue)
        if tag == Labels.DIPL:
            result = IssueParser.__parse_dipl(issue)
        if tag == Labels.KURS:
            result = IssueParser.__parse_kurs(issue)
        return result

    @staticmethod
    def __parse_dipl(issue: Issue) -> Dict:
        """Method to get info from diplom issue

        Args:
            issue (Issue): issue with theme

        Returns:
            Dict: info from diplom issue
        """
        result = {
            "full_author": {"title": "", "details": ""},
            "theme": {"title": "", "details": ""},
            "quests": {"title": "", "details": []},
            "annotation": {"title": "", "details": ""},
            "recomendation": {"title": "", "details": []},
            "target": {"title": "", "details": ""},
            "unknown": [],
            "warnings": [],
        }

        description = issue.description.split("\n")
        title = None

        for desc in description:
            if not desc:
                continue
            if desc[:2] == IssueParser.title_start:
                title = IssueParser.__parse_description_title(desc)
                if title.warning:
                    result["warnings"].append(title.warning)
                    result["unknown"].append(title.title)
                else:
                    result[title.tag]["title"] = title.title
            else:
                if not title:
                    warning = Warnings.UNKNOWN + desc
                    result["warnings"].append(warning)
                    result["unknown"].append(desc)
                if title:
                    if title.tag in ("quests", "recomendation"):
                        result[title.tag]["details"].append(desc)
                    elif not result[title.tag]["details"]:
                        result[title.tag]["details"] = desc
                    else:
                        warning = Warnings.UNKNOWN + desc
                        result["warnings"].append(warning)
                        result["unknown"].append(desc)
        result = IssueParser.__post_parse(result)
        return result

    @staticmethod
    def __post_parse(result: dict) -> Dict:
        """Method to post parse checking empty titles

        Args:
            result (dict): result dict

        Returns:
            Dict: result with warning if any title is empty
        """
        for part in result.items():
            if not part[1]["title"]:
                warning = Warnings.MISSING + part[0]
                result["warnings"].append(warning)
        return result

    @staticmethod
    def __parse_description_title(desc: str) -> Title:
        """Method to get title from description

        Args:
            desc (str): title description

        Returns:
            Title: tag, title and warning if something gonna wrong
        """
        warning = ""
        title = ""
        tag = "unknown"

        patterns = {
            "annotation": IssueParser.annotation,
            "full_author": IssueParser.author,
            "quests": IssueParser.quests,
            "recomendation": IssueParser.recomendation,
            "target": IssueParser.target,
            "theme": IssueParser.theme,
        }

        for pattern in patterns.items():
            if search(pattern[1], desc):
                title = desc
                tag = pattern[0]
                break
        if not title:
            warning = Warnings.TITLE + desc
            title = desc

        return Title(tag, title, warning)

    @staticmethod
    def __parse_kurs(issue: Issue) -> Dict:
        """Method to get info from kurs issue

        Args:
            issue (Issue): issue with theme

        Returns:
            Dict: info from kurs issue
        """
        return {}

    @staticmethod
    def __get_type(issue: Issue) -> Labels:
        """Method to get type of issue

        Args:
            issue (Issue): issue with theme

        Returns:
            Labels: dipl/kurs
        """
        tags = issue.labels
        if not bool(Labels.DIPL in tags) ^ bool(Labels.KURS in tags):
            return Labels.UNKNOWN
        return Labels.DIPL if Labels.DIPL in tags else Labels.KURS


def get_issues(link: str, access_token: str) -> List[Issue]:
    """Funtion to get issues from project

    Args:
        link (str): project url
        access_token (str): user private token

    Returns:
        list[gitlab.v4.objects.Issues]: issues to parse
    """
    project = process_group(link, access_token)
    issues = project.issues.list()
    return issues


def parse_issue(link: str, access_token: str, *args) -> List[str]:
    """Function to parse issues to JSON objects

    Args:
        link (str): project url
        access_token (str): user private token
    Returns:
        List[str]: list of parsed JSON
    """
    issues = get_issues(link, access_token)
    for issue in issues:
        title = IssueParser.parse_meta(issue)
        info = IssueParser.parse_info(issue)
        title["details"] = info
        print(title)
