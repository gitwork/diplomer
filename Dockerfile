FROM python:3.8.3-alpine

COPY requirements.txt .
RUN pip install -r requirements.txt

RUN mkdir /backend
COPY /backend /backend
RUN echo ${ACTION}

CMD ["python3", "backend/kidp_themes.py"]
